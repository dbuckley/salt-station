---
#==============================================================================
# Install Sway and required drivers
#==============================================================================
sway_repo:
  pkgrepo.managed:
    - humanname: Sway Copr Repo $releasever
    - baseurl: https://copr-be.cloud.fedoraproject.org/results/geoffbeier/sway/fedora-$releasever-$basearch/
    - enabled: true
    - gpgcheck: 1
    - gpgkey: https://copr-be.cloud.fedoraproject.org/results/geoffbeier/sway/pubkey.gpg
sway:
  pkg.installed:
    - pkgs:
      - sway
      - swayidle
      - swaylock
      - wlroots
      - mesa-dri-drivers
      - mesa-vulkan-drivers
      - xorg-x11-server-Xwayland
    - requires:
      - sway_repo

#==============================================================================
# Settup Alacritty
#==============================================================================
alacritty_repo:
  pkgrepo.managed:
    - humanname: Alacritty Copr Repo $releasever
    - baseurl: https://copr-be.cloud.fedoraproject.org/results/cesp/alacritty/fedora-$releasever-$basearch/
    - enabled: true
    - gpgcheck: 1
    - gpgkey: https://copr-be.cloud.fedoraproject.org/results/cesp/alacritty/pubkey.gpg
alacritty:
  pkg:
    - installed
    - requires:
      - alacritty_repo

#==============================================================================
# Install fonts and other supporting packages
#==============================================================================
sway_pkgs:
  pkg.installed:
    - pkgs:
      - waybar
      - overpass-mono-fonts
      - fontawesome-fonts
    - requires:
      - sway_repo
