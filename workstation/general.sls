---
#==============================================================================
# Ensure I exist
#==============================================================================
dbuckley:
  group:
    - present
    - gid: 82894
  user:
    - present
    - uid: 82894
    - require:
        - group: dbuckley

#==============================================================================
# Install General Packages
#==============================================================================
base_pkgs:
  pkg.installed:
    - pkgs:
      - firefox-wayland
      - fzf
      - light
      - neovim
      - psmisc
      - pulseaudio
      - rcm
      - tmux

#==============================================================================
# Setup Dotfiles
#==============================================================================
dotfiles:
  git.latest:
    - name: https://gitlab.com/dbuckley/dotfiles
    - branch: master
    - target: /home/dbuckley/.dotfiles
    - user: dbuckley
dotfiles_rc:
  cmd.run:
    - name: 'cd /home/dbuckley/.dotfiles && rcup -f'
    - user: dbuckley
    - onchanges:
        - dotfiles
    - require:
      - pkg: base_pkgs

#==============================================================================
# Setup vimrc
#==============================================================================
neovim_pip:
  pip.installed:
    - name: neovim
    - bin_env: '/usr/bin/pip3'
pynvim_pip:
  pip.installed:
    - name: pynvim
    - bin_env: '/usr/bin/pip3'
vim_repo:
  git.latest:
    - name: https://gitlab.com/dbuckley/vim
    - branch: master
    - target: /home/dbuckley/.vim
    - user: dbuckley
    - submodules: True
/home/dbuckley/.vimrc:
  file.symlink:
    - target: /home/dbuckley/.vim/vimrc
    - user: dbuckley
    - group: dbuckley
    - mode: 644
    - makedirs: True
    - force: True
/home/dbuckley/.config/nvim:
  file.symlink:
    - target: /home/dbuckley/.vim
    - user: dbuckley
    - group: dbuckley
    - mode: 644
    - makedirs: True
    - force: True
/home/dbuckley/.config/nvim/init.vim:
  file.symlink:
    - target: /home/dbuckley/.vim/vimrc
    - user: dbuckley
    - group: dbuckley
    - mode: 644
    - makedirs: True
    - force: True
LanguageClient_Bin:
  file.directory:
    - name: /home/dbuckley/.vim/bundle/LanguageClient-neovim/bin
    - user: dbuckley
    - group: dbuckley
/home/dbuckley/.vim/bundle/LanguageClient-neovim/bin/languageclient:
  file.managed:
    - source: https://github.com/autozimu/LanguageClient-neovim/releases/download/0.1.154/languageclient-0.1.154-aarch64-unknown-linux-gnu
    - source_hash: fce0f5177567165b4ffa47beea3459e196b105f9171ac32a193e6de66248252c
    - user: dbuckley
    - group: dbuckley
    - mode: 755
