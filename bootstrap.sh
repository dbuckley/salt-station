#!/bin/sh -
#
#       CREATED : September 27, 2019
#          FILE : bootstrap-salt.sh
#   DESCRIPTION : Bootstrap Salt installation for PC maintenance (workstations
#                   & laptops)
#     COPYRIGHT : (c) 2019 by Derek Buckley
#       LICENSE : MPL 2.0
#==============================================================================

#==============================================================================
# Functions
#==============================================================================

#####
# Log is used to standardize echo log
log() {
	echo "> $@"
}

#####
# Spinner for a nice waiting icon.
# Will wait for pid given as pos $1
spinner() {
	local pid=$1
	local delay=0.2
	local spin='|/-\'
	while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
		local tmp=${spin#?}
		printf " [%c]  " "$spin"
		local spin=$tmp${spin%"$temp"}
		sleep $delay
		printf "\b\b\b\b\b\b"
	done
	printf "    \b\b\b\b"
}

#####
# Helper function to download the bootstrap script and ask the user to inspect
# before running the install
get_bootstrap() {
	curl -L https://bootstrap.saltstack.com -o $1
	echo
	echo "Please validate the salt install script at"
	echo $1.
	read -p "Continue: [y/N]" resp
	case $resp in
		[yY]|[yY][eE][sS])
			return
			;;
		*)
			exit 0
	esac
}

#####
# Start minion service and print message
salt_start() {
	systemctl start salt-minion.service > /dev/null
	log Started salt-minion.service
}

#==============================================================================
# Initial Checks
#==============================================================================

#####
# Validate running as root
if [[  `id -u` != 0 ]]; then
	>&2 log "Please run this script as root"
	exit 1
fi

#==============================================================================
# Run The Bootstrap from SaltStack then complete setup
#==============================================================================

#####
# Run bootstrap_salt.sh if salt is absent
BOOTSTRAP_SCRIPT=/tmp/bootstrap_salt.sh
if which salt-minion > /dev/null; then
	log Salt minion is already installed. Skipping install
else
	log Downloading $BOOTSTRAP_SCRIPT
	get_bootstrap $BOOTSTRAP_SCRIPT
	log Running the $BOOTSTRAP_SCRIPT...
	sh $BOOTSTRAP_SCRIPT & spinner $!
fi

#####
# Ensure config is set correctly
sed -i 's/^.file_client:.*$/file_client: local/' /etc/salt/minion
sed -i 's/^.*master_type:.*$/master_type: disable/' /etc/salt/minion

####
# Ensure salt-station is cloned under /srv where salt-minion expects it
ls /srv/salt > /dev/null 2>&1 || git clone https://gitlab.com/dbuckley/salt-station /srv/salt

#####
# Ensure salt is running
pgrep salt-minion > /dev/null || salt_start
log "Salt minion is installed and running"
