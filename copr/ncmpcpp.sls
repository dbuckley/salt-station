---
#==============================================================================
# Install Sway and required drivers
#==============================================================================
ncmpcpp_repo:
  pkgrepo.managed:
    - humanname: Sway Copr Repo $releasever
    - baseurl: https://copr-be.cloud.fedoraproject.org/results/jstanek/ports/fedora-$releasever-$basearch/
    - enabled: true
    - gpgcheck: 1
    - gpgkey: https://copr-be.cloud.fedoraproject.org/results/jstanek/ports/pubkey.gpg
ncmpcpp:
  pkg.installed:
    - pkgs:
        - ncmpcpp-spectrum
    - requires:
        - ncmpcpp_repo
