---
#==============================================================================
# Setup Saltstation Files
#==============================================================================
salt-station-home:
  git.cloned:
    - name: https://gitlab.com/dbuckley/salt-station
    - branch: master
    - target: /home/dbuckley/src/salt-station

salt-station-src:
  git.latest:
    - name: /home/dbuckley/src/salt-station
    - branch: master
    - target: /srv/salt
    - force_checkout: true
    - require:
      - salt-station-home
