# Salt Station

## Overview

This repo is a test of using saltstack to manage my workstation and laptops.
The goal is to get familiar with saltstack and to see if it is more enjoyable
of a config managment tool then my current solution of
[Ansible](https://gitlab.com/dbuckley/workstation-deploy).
