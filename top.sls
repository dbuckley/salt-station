---
base:
  '*':
    - bootstrap
    - workstation.general
    - workstation.sway
  'apollo.*,prometheus.*':
    - workstation.mpd
